import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.Stack;

public class Palindrome {
	 public static void main (String[] args) throws NoSuchElementException{ 
		
		 Stack <String> stack = new Stack <String> (); 
		
		 LinkedList <String> queue = new LinkedList<String> (); 
		 Scanner scanner = new Scanner(System.in); 
		 System.out.print("Enter text: "); 
		 String inputStr = scanner.next(); 
		 for (int i=0; i < inputStr.length(); i++) { 
			 String ch = inputStr.substring(i, i+1); 
			 stack.push(ch); 
			 queue.offer(ch); } 
		 
 boolean ans = true; 
 try { 
	 while (!stack.isEmpty() && ans) { 
		 if (!(stack.pop().equals(queue.poll()))) 
			 ans = false; 
		 } 
	 } catch (NoSuchElementException e) { 
		 throw new NoSuchElementException(); 
		 } 
 System.out.print(inputStr + " is "); 
 if (ans) 
	 System.out.println("a palindrome"); 
 else System.out.println("NOT a palindrome"); 
 }
}

